<?php

declare(strict_types = 1);

namespace App\Factory;

use App\Model\CommitDataModel;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
class CommitDataModelFactory
{
    /**
     * @param string $sha
     * @return CommitDataModel
     */
    public static function create(string $sha): CommitDataModel
    {
        $commitDataModel = new CommitDataModel();
        $commitDataModel->setSha($sha);

        return $commitDataModel;
    }
}
