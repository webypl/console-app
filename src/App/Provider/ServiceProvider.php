<?php

declare(strict_types = 1);

namespace App\Provider;

use App\Enum\ServiceEnum;
use App\Exception\ServiceNotFoundException;
use App\Service\ServiceInterface;
use ReflectionException;
use function in_array;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * @var array
     */
    private $services = [];
    /**
     * {@inheritdoc}
     * @throws ServiceNotFoundException
     * @throws ReflectionException
     */
    public function getService(string $key = ''): ServiceInterface
    {
        if (false === in_array($key, ServiceEnum::getAvailableServices(), true)) {
            throw new ServiceNotFoundException('Service not found');
        }

        return $this->services[$key];
    }

    /**
     * {@inheritdoc}
     */
    public function addService(string $key, ServiceInterface $service): void
    {
        $this->services[$key] = $service;
    }
}
