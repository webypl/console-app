<?php

declare(strict_types = 1);

namespace App\Provider;

use App\Service\ServiceInterface;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
interface ServiceProviderInterface
{
    /**
     * @param string $name
     * @return ServiceInterface
     */
    public function getService(string $name = ''): ServiceInterface;

    /**
     * @param string           $key
     * @param ServiceInterface $service
     */
    public function addService(string $key, ServiceInterface $service): void;
}
