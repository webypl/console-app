<?php

declare(strict_types = 1);

namespace App\Exception;

use Exception;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
class ServiceNotFoundException extends Exception
{

}
