<?php

declare(strict_types = 1);

namespace App\Command;

use App\Enum\ServiceEnum;
use App\Provider\ServiceProvider;
use App\Service\GitHubService;
use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Exception;
use splitbrain\phpcli\Options;
use function in_array;
use function sprintf;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
class LastCommitCommand extends CLI
{
    protected function setup(Options $options): void
    {
        $options->setHelp('Get last repository commit sha');

        $options->registerArgument('repository', 'Repository name eg. owner/repo');
        $options->registerArgument('branch', 'BranchName');

        $options->registerOption(
            'service',
            sprintf('Service name: %s', implode(', ', ServiceEnum::getAvailableServices())),
            null,
            'service key'
        );
    }

    protected function main(Options $options): void
    {
        // this should be managed by Dependency Injection / IoC or similar
        $serviceProvider = new ServiceProvider();
        $serviceProvider->addService(ServiceEnum::GITHUB, new GitHubService());

        $args = $options->getArgs();
        $serviceOption = $this->options->getOpt('service', ServiceEnum::GITHUB);

        $service = $serviceProvider->getService($serviceOption);

        echo sprintf('%s %s', $service->getLastCommitSha($args[0], $args[1]), PHP_EOL);
    }

    protected function checkArgments(): void
    {
        $args = $this->options->getArgs();

        if (true !== isset($args[0])) {
            throw new Exception('Repository name is required', Exception::E_OPT_ARG_REQUIRED);
        }

        if (true !== isset($args[0])) {
            throw new Exception('Branch name is required', Exception::E_OPT_ARG_REQUIRED);
        }

        $serviceOption = $this->options->getOpt('service', ServiceEnum::GITHUB);

        if (true !== in_array($serviceOption, ServiceEnum::getAvailableServices(), true)) {
            throw new Exception(sprintf('Unknown service \'%s\'', $serviceOption));
        }
    }
}
