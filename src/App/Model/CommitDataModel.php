<?php

declare(strict_types = 1);

namespace App\Model;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
class CommitDataModel
{
    /**
     * @var string
     */
    private $sha;

    /**
     * @return string
     */
    public function getSha(): string
    {
        return $this->sha;
    }

    /**
     * @param string $sha
     */
    public function setSha(string $sha): void
    {
        $this->sha = $sha;
    }
}
