<?php

declare(strict_types = 1);

namespace App\Enum;

use ReflectionClass;
use ReflectionException;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
class ServiceEnum
{
    public const GITHUB = 'github';

    /**
     * @return array
     * @throws ReflectionException
     */
    public static function getAvailableServices(): array
    {
        $oClass = new ReflectionClass(__CLASS__);

        return $oClass->getConstants();
    }
}
