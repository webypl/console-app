<?php

declare(strict_types = 1);

namespace App\Service;

use App\Exception\CommitDataNotFoundException;
use App\FActory\CommitDataModelFactory;
use App\Model\CommitDataModel;
use function  file_get_contents;
use function sprintf;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
class GitHubService extends AbstractService
{
    private const  URL = 'https://api.github.com/repos/%s/git/refs/heads/%s';

    protected function getLastCommitData(string $repository, string $branch): CommitDataModel
    {
        $url = sprintf(self::URL, $repository, $branch);

        $opts = [
            'http' => [
                'method' => 'GET',
                'header' => [
                    'User-Agent: PHP',
                ],
            ],
        ];

        $context = stream_context_create($opts);
        $content = @file_get_contents($url, false, $context);

        if (false === $content) {
            throw new CommitDataNotFoundException('Commit data not found');
        }

        $data = json_decode($content, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new CommitDataNotFoundException('Unreadable data');
        }

        if (true !== isset($data['object']['sha'])) {
            throw new CommitDataNotFoundException('Sha not found');
        }


        return CommitDataModelFactory::create($data['object']['sha']);
    }
}
