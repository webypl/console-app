<?php

declare(strict_types = 1);

namespace App\Service;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
interface ServiceInterface
{
    /**
     * @param string $repository
     * @param string $branch
     * @return string
     */
    public function getLastCommitSha(string $repository, string $branch): string;
}
