<?php

declare(strict_types = 1);

namespace App\Service;

use App\Exception\CommitDataNotFoundException;
use App\Model\CommitDataModel;

/**
 * @author Łukasz Łukasiewicz <lukasz@weby.pl>
 */
abstract class AbstractService implements ServiceInterface
{
    /**
     * {@inheritdoc}
     * @throws CommitDataNotFoundException
     */
    public function getLastCommitSha(string $repository, string $branch): string
    {
        $commitDataModel = $this->getLastCommitData($repository, $branch);

        return $commitDataModel->getSha();
    }

    /**
     * @param string $repository
     * @param string $branch
     * @return CommitDataModel
     * @throws CommitDataNotFoundException
     */
    abstract protected function getLastCommitData(string $repository, string $branch): CommitDataModel;
}
